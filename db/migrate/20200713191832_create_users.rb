class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password_digest
      t.date :created_at2
      t.string :role

      t.timestamps
    end
  end
end
